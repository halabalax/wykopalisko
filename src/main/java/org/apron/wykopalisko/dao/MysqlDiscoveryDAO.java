package org.apron.wykopalisko.dao;

import org.apron.wykopalisko.model.Discovery;
import org.apron.wykopalisko.model.User;
import org.apron.wykopalisko.model.VoteType;
import org.apron.wykopalisko.util.ConnectionProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

public class MysqlDiscoveryDAO implements DiscoveryDAO {

    private static final Logger logger = LoggerFactory.getLogger(MysqlDiscoveryDAO.class);
    private static final String CREATE_DISCOVERY =
            "INSERT INTO discovery(name, description, url, user_id, date) "
                    + "VALUES(:name, :description, :url, :user_id, :date);";
    private static final String READ_ALL_DISCOVERIES =
            "SELECT user.user_id, username, email, is_active, discovery_id, name, description, url, date "
                    + "FROM discovery LEFT JOIN user ON discovery.user_id=user.user_id;";
    private static final String READ_VOTES_FOR_DISCOVERY = "SELECT type FROM vote WHERE discovery_id=:discovery_id;";
    private static final String READ_DISCOVERY =
            "SELECT user.user_id, username, email, is_active, password, discovery_id, name, description, url, date "
                    + "FROM discovery LEFT JOIN user ON discovery.user_id=user.user_id WHERE discovery_id=:discovery_id;";
    private static final String UPDATE_DISCOVERY =
            "UPDATE discovery SET name=:name, description=:description, url=:url, user_id=:user_id, date=:date "
                    + "WHERE discovery_id=:discovery_id;";
    private static final String DESCRIPTION = "description";
    private static final String USER_ID = "user_id";
    private static final String NAME = "name";
    private static final String URL = "url";
    private static final String DATE = "date";
    private static final String DISCOVERY_ID = "discovery_id";

    private NamedParameterJdbcTemplate template;

    public MysqlDiscoveryDAO() {
        try {
            this.template = new NamedParameterJdbcTemplate(ConnectionProvider.getDataSource());
        } catch (SQLException e) {
            logger.error("", e);
        }
    }

    @Override
    public Discovery create(Discovery discovery) {
        Discovery resultDiscovery = new Discovery(discovery);
        KeyHolder holder = new GeneratedKeyHolder();
        Map<String, Object> paramMap = getMapWithDiscoveryToItsKeys(discovery);
        SqlParameterSource paramSource = new MapSqlParameterSource(paramMap);
        int update = template.update(CREATE_DISCOVERY, paramSource, holder);
        if (update > 0) {
            resultDiscovery.setId(Objects.requireNonNull(holder.getKey()).longValue());
        }
        return resultDiscovery;
    }

    private Map<String, Object> getMapWithDiscoveryToItsKeys(Discovery discovery) {
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put(NAME, discovery.getName());
        paramMap.put(DESCRIPTION, discovery.getDescription());
        paramMap.put(URL, discovery.getUrl());
        paramMap.put(USER_ID, discovery.getUser().getId());
        paramMap.put(DATE, discovery.getTimestamp());
        return paramMap;
    }

    @Override
    public Discovery read(Long primaryKey) {
        SqlParameterSource paramSource = new MapSqlParameterSource(DISCOVERY_ID, primaryKey);
        return template.queryForObject(READ_DISCOVERY, paramSource, new DiscoveryRowMapper());
    }

    @Override
    public boolean update(Discovery discovery) {
        boolean result = false;
        Map<String, Object> paramMap = getMapWithDiscoveryToItsKeys(discovery);
        paramMap.put(DISCOVERY_ID, discovery.getId());
        SqlParameterSource paramSource = new MapSqlParameterSource(paramMap);
        int update = template.update(UPDATE_DISCOVERY, paramSource);
        if (update > 0) {
            result = true;
        }
        return result;
    }

    @Override
    public boolean delete(Long primaryKey) {
        return false;
    }

    @Override
    public List<Discovery> getAll() {
        List<Discovery> discoveries = template.query(READ_ALL_DISCOVERIES, new DiscoveryRowMapper());
        for (Discovery discovery : discoveries) {
            SqlParameterSource paramSource = new MapSqlParameterSource(DISCOVERY_ID, discovery.getId());
            List<VoteType> voteTypes = template.query(READ_VOTES_FOR_DISCOVERY, paramSource, new VoteTypeRowMapper());
            Map<VoteType, Integer> voteTypeCounts = new EnumMap<>(VoteType.class);
            for (VoteType voteType : voteTypes) {
                int prevVal = voteTypeCounts.computeIfAbsent(voteType, vote -> 0);
                voteTypeCounts.put(voteType, ++prevVal);
            }
            discovery.setDownVote(voteTypeCounts.getOrDefault(VoteType.VOTE_DOWN, 0));
            discovery.setUpVote(voteTypeCounts.getOrDefault(VoteType.VOTE_UP, 0));
        }
        return discoveries;
    }

    private static class VoteTypeRowMapper implements RowMapper<VoteType> {
        @Override
        public VoteType mapRow(ResultSet resultSet, int row) throws SQLException {
            return VoteType.from(resultSet.getString("type")).orElseThrow(() -> new SQLException("Unknown vote type!"));
        }
    }

    private static class DiscoveryRowMapper implements RowMapper<Discovery> {
        @Override
        public Discovery mapRow(ResultSet resultSet, int row) throws SQLException {
            Discovery discovery = new Discovery();
            discovery.setId(resultSet.getLong(DISCOVERY_ID));
            discovery.setName(resultSet.getString("name"));
            discovery.setDescription(resultSet.getString(DESCRIPTION));
            discovery.setUrl(resultSet.getString(URL));
            discovery.setTimestamp(resultSet.getTimestamp(DATE));
            User user = new User();
            user.setId(resultSet.getLong(USER_ID));
            user.setUsername(resultSet.getString("username"));
            user.setEmail(resultSet.getString("email"));
            discovery.setUser(user);
            return discovery;
        }
    }
}
