package org.apron.wykopalisko.dao;

public class MysqlDAOFactory extends DAOFactory {
    @Override
    public DiscoveryDAO getDiscoveryDAO() {
        return new MysqlDiscoveryDAO();
    }

    @Override
    public UserDAO getUserDAO() {
        return new MysqlUserDAO();
    }

    @Override
    public VoteDAO getVoteDAO() {
        return new MysqlVoteDAO();
    }
}
