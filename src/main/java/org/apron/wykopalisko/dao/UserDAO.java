package org.apron.wykopalisko.dao;

import org.apron.wykopalisko.model.User;

public interface UserDAO extends GenericDAO<User, Long> {
    User getUserByUserName(String username);
}
