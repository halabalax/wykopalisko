package org.apron.wykopalisko.dao;

import org.apron.wykopalisko.model.Discovery;

import java.util.List;

public interface DiscoveryDAO extends GenericDAO<Discovery, Long> {
    List<Discovery> getAll();
}
