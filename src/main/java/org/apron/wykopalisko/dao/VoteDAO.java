package org.apron.wykopalisko.dao;

import org.apron.wykopalisko.model.Vote;

import java.util.Optional;

public interface VoteDAO extends GenericDAO<Vote, Long> {
    Optional<Vote> getVoteByUserIdDiscoveryId(long userId, long discoveryId);
}
