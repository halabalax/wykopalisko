package org.apron.wykopalisko.dao;

import java.io.Serializable;

public interface GenericDAO<T, K extends Serializable> {
    T create(T newObject);
    T read(K primaryKey);
    boolean update(T updateObject);
    boolean delete(K primaryKey);
}
