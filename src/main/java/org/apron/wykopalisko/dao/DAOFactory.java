package org.apron.wykopalisko.dao;

import org.apron.wykopalisko.exception.NoSuchDbTypeException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class DAOFactory {

    public static final int MYSQL_DAO_FACTORY = 1;

    public abstract DiscoveryDAO getDiscoveryDAO();

    public abstract UserDAO getUserDAO();

    public abstract VoteDAO getVoteDAO();

    private static final Logger logger = LoggerFactory.getLogger(DAOFactory.class);

    public static DAOFactory getDAOFactory() {
        DAOFactory factory = null;
        try {
            factory = getDAOFactory(MYSQL_DAO_FACTORY);
        } catch (NoSuchDbTypeException e) {
            logger.error("", e);
        }
        return factory;
    }

    private static DAOFactory getDAOFactory(int type) throws NoSuchDbTypeException {
        if (type == MYSQL_DAO_FACTORY) {
            return new MysqlDAOFactory();
        } else {
            throw new NoSuchDbTypeException();
        }
    }
}
