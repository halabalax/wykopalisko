package org.apron.wykopalisko.dao;

import org.apron.wykopalisko.model.Vote;
import org.apron.wykopalisko.model.VoteType;
import org.apron.wykopalisko.util.ConnectionProvider;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class MysqlVoteDAO implements VoteDAO {

    private static final String CREATE_VOTE = "INSERT INTO vote(discovery_id, user_id, date, type) "
            + "VALUES (:discovery_id, :user_id, :date, :type);";
    private static final String READ_VOTE = "SELECT vote_id, discovery_id, user_id, date, type "
            + "FROM vote WHERE vote_id = :vote_id;";
    private static final String READ_VOTE_BY_DISCOVERY_USE_IDS = "SELECT vote_id, discovery_id, user_id, date, type "
            + "FROM vote WHERE user_id = :user_id AND discovery_id = :discovery_id;";
    private static final String UPDATE_VOTE = "UPDATE vote SET date=:date, type=:type WHERE vote_id=:vote_id;";
    private static final String DISCOVERY_ID = "discovery_id";
    private static final String VOTE_ID = "vote_id";
    private static final String USER_ID = "user_id";

    private NamedParameterJdbcTemplate template;

    public MysqlVoteDAO() {
        try {
            this.template = new NamedParameterJdbcTemplate(ConnectionProvider.getDataSource());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Optional<Vote> getVoteByUserIdDiscoveryId(long userId, long discoveryId) {
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put(USER_ID, userId);
        paramMap.put(DISCOVERY_ID, discoveryId);
        SqlParameterSource paramSource = new MapSqlParameterSource(paramMap);
        try {
            return Optional.of(template.queryForObject(READ_VOTE_BY_DISCOVERY_USE_IDS, paramSource, new VoteRowMapper()));
        } catch(EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }

    @Override
    public Vote create(Vote vote) {
        Vote voteCopy = new Vote(vote);
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put(DISCOVERY_ID, voteCopy.getDiscoveryId());
        paramMap.put(USER_ID, voteCopy.getUserId());
        paramMap.put("date", voteCopy.getDate());
        paramMap.put("type", voteCopy.getVoteType().toString());
        KeyHolder holder = new GeneratedKeyHolder();
        SqlParameterSource paramSource = new MapSqlParameterSource(paramMap);
        int update = template.update(CREATE_VOTE, paramSource, holder);
        if(update > 0) {
            voteCopy.setId(holder.getKey().longValue());
        }
        return voteCopy;
    }

    @Override
    public Vote read(Long primaryKey) {
        SqlParameterSource paramSource = new MapSqlParameterSource(VOTE_ID, primaryKey);
        return template.queryForObject(READ_VOTE, paramSource, new VoteRowMapper());
    }

    @Override
    public boolean update(Vote vote) {
        boolean result = false;
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("date", vote.getDate());
        paramMap.put("type", vote.getVoteType().toString());
        paramMap.put(VOTE_ID, vote.getId());
        SqlParameterSource paramSource = new MapSqlParameterSource(paramMap);
        int update = template.update(UPDATE_VOTE, paramSource);
        if(update > 0) {
            result = true;
        }
        return result;
    }

    @Override
    public boolean delete(Long primaryKey) {
        return false;
    }

    private static class VoteRowMapper implements RowMapper<Vote> {
        @Override
        public Vote mapRow(ResultSet resultSet, int row) throws SQLException {
            Vote vote = new Vote();
            vote.setId(resultSet.getLong(VOTE_ID));
            vote.setUserId(resultSet.getLong(USER_ID));
            vote.setDiscoveryId(resultSet.getLong(DISCOVERY_ID));
            vote.setDate(resultSet.getTimestamp("date"));
            vote.setVoteType(VoteType.from(resultSet.getString("type")).orElseThrow(() -> new SQLException("Invalid vote type!")));
            return vote;
        }
    }
}
