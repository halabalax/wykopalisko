package org.apron.wykopalisko.controller;

import org.apron.wykopalisko.model.User;
import org.apron.wykopalisko.model.VoteType;
import org.apron.wykopalisko.service.VoteService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

@WebServlet("/vote")
public class VoteController extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        User loggedUser = (User) request.getSession().getAttribute("user");
        if (loggedUser != null) {
            Optional<VoteType> voteType = VoteType.from(request.getParameter("vote"));
            if (voteType.isPresent()) {
                long userId = loggedUser.getId();
                long discoveryId = Long.parseLong(request.getParameter("discovery_id"));
                updateVote(userId, discoveryId, voteType.get());
            }
        }
        response.sendRedirect(request.getContextPath() + "/");
    }

    private void updateVote(long userId, long discoveryId, VoteType voteType) {
        VoteService voteService = new VoteService();
        voteService.addOrUpdateVote(discoveryId, userId, voteType);
    }
}
