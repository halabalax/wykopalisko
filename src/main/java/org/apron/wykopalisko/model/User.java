package org.apron.wykopalisko.model;

import lombok.Data;

import java.io.Serializable;

@Data
public class User implements Serializable {
    private long id;
    private String username;
    private String email;
    private String password;
    private boolean active;

    public User() {
    }

    public User(User user) {
        id = user.id;
        username = user.username;
        email = user.email;
        password = user.password;
        active = user.active;
    }
}
