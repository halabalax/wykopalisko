package org.apron.wykopalisko.model;

import lombok.Data;

import java.sql.Timestamp;

@Data
public class Discovery {
    private long id;
    private String name;
    private String description;
    private String url;
    private Timestamp timestamp;
    private User user;
    private int upVote;
    private int downVote;

    public Discovery() {
    }

    public Discovery(Discovery discovery) {
        this.id = discovery.id;
        this.name = discovery.name;
        this.description = discovery.description;
        this.url = discovery.url;
        this.timestamp = discovery.timestamp;
        this.user = discovery.user;
        this.upVote = discovery.upVote;
        this.downVote = discovery.downVote;
    }
}
