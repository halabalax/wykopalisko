package org.apron.wykopalisko.model;

import java.util.Arrays;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

public enum VoteType {
    VOTE_UP, VOTE_DOWN;

    private static final Map<String, VoteType> STRING_TO_VOTE_TYPE = Arrays.stream(VoteType.values())
            .collect(Collectors.toMap(Object::toString, voteType -> voteType));

    public static Optional<VoteType> from(String voteTypeAsString) {
        return Optional.ofNullable(STRING_TO_VOTE_TYPE.get(voteTypeAsString));
    }
}
