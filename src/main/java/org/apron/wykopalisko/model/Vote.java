package org.apron.wykopalisko.model;

import lombok.Data;

import java.sql.Timestamp;

@Data
public class Vote {
    private long id;
    private long discoveryId;
    private long userId;
    private Timestamp date;
    private VoteType voteType;

    public Vote() {
    }

    public Vote(Vote vote) {
        this.id = vote.id;
        this.discoveryId = vote.discoveryId;
        this.userId = vote.userId;
        this.date = vote.date;
        this.voteType = vote.voteType;
    }
}
