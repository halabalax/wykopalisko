package org.apron.wykopalisko.util;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import javax.validation.constraints.NotNull;
import java.sql.Connection;
import java.sql.SQLException;

public class ConnectionProvider {
    private static DataSource dataSource;

    private ConnectionProvider() {
    }

    public static Connection getConnection() throws SQLException {
        return getDataSource().getConnection();
    }

    @NotNull
    public static DataSource getDataSource() throws SQLException {
        if(dataSource == null) {
            try {
                Context initialContext = new InitialContext();
                Context envContext = (Context) initialContext.lookup("java:comp/env");
                dataSource = (DataSource) envContext.lookup("jdbc/wykopalisko");
            }catch (NamingException e) {
                throw new SQLException(e);
            }
        }
        return dataSource;
    }
}
