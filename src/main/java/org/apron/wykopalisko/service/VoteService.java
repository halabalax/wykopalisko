package org.apron.wykopalisko.service;

import org.apron.wykopalisko.dao.DAOFactory;
import org.apron.wykopalisko.dao.VoteDAO;
import org.apron.wykopalisko.model.Vote;
import org.apron.wykopalisko.model.VoteType;

import java.sql.Timestamp;
import java.util.Date;
import java.util.Optional;

public class VoteService {
    public Vote addVote(long discoveryId, long userId, VoteType voteType) {
        Vote vote = new Vote();
        vote.setDiscoveryId(discoveryId);
        vote.setUserId(userId);
        vote.setDate(new Timestamp(new Date().getTime()));
        vote.setVoteType(voteType);
        DAOFactory factory = DAOFactory.getDAOFactory();
        VoteDAO voteDao = factory.getVoteDAO();
        vote = voteDao.create(vote);
        return vote;
    }

    public Optional<Vote> updateVote(long discoveryId, long userId, VoteType voteType) {
        DAOFactory factory = DAOFactory.getDAOFactory();
        VoteDAO voteDao = factory.getVoteDAO();
        return voteDao.getVoteByUserIdDiscoveryId(userId, discoveryId).flatMap(voteToUpdate ->
                Optional.of(updateVote(voteToUpdate,voteType)));
    }
    private Vote updateVote(Vote vote, VoteType voteType) {
        DAOFactory factory = DAOFactory.getDAOFactory();
        VoteDAO voteDao = factory.getVoteDAO();
        vote.setVoteType(voteType);
        voteDao.update(vote);
        return vote;
    }

    public Vote addOrUpdateVote(long discoveryId, long userId, VoteType voteType) {
        DAOFactory factory = DAOFactory.getDAOFactory();
        VoteDAO voteDao = factory.getVoteDAO();
        Optional<Vote> vote = voteDao.getVoteByUserIdDiscoveryId(userId, discoveryId);
        return vote.map(v -> updateVote(v, voteType)).orElseGet(() -> addVote(discoveryId, userId, voteType));
    }

    public Optional<Vote> getVoteByDiscoveryUserId(long discoveryId, long userId) {
        DAOFactory factory = DAOFactory.getDAOFactory();
        VoteDAO voteDao = factory.getVoteDAO();
        return voteDao.getVoteByUserIdDiscoveryId(userId, discoveryId);
    }
}
