<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html class="h-100" lang="pl">
<head>
    <title>Wykopalisko</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" type="text/css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/callout.css" type="text/css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/styles.css" type="text/css" rel="stylesheet">
</head>

<body class="d-flex flex-column h-100">
<jsp:include page="fragment/header.jspf"/>
<main role="main" class="flex-shrink-0">
    <div class="container">
        <div class="col-sm-6 col-md-4 offset-md-4">
            <form class="form-signin text-center" action="register" method="post">
                <h1 class="h3 mb-3 font-weight-normal">Rejestracja</h1>
                <div class="form-group">
                    <label for="inputEmail" class="sr-only">Email</label>
                    <input class="form-control" name="inputEmail" id="inputEmail" type="email" placeholder="Email" required autofocus/>
                    <label for="inputUsername" class="sr-only">Nazwa użytkownika</label>
                    <input class="form-control" name="inputUsername" id="inputUsername" type="text" placeholder="Nazwa użytkownika" required autofocus/>
                    <label for="inputPassword" class="sr-only">Hasło</label>
                    <input class="form-control" name="inputPassword" id="inputPassword" type="password" placeholder="Hasło" required/>
                </div>
                <button class="btn btn-primary btn-lg btn-block" type="submit">Zarejestruj się</button>
            </form>
        </div>
    </div>
</main>
<jsp:include page="fragment/footer.jspf"/>
<script src="http://code.jquery.com/jquery-1.11.2.min.js"></script>
<script src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/bootstrap.bundle.min.js"></script>
</body>
</html>