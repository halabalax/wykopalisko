<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html class="h-100" lang="pl">
<head>
    <title>Wykopalisko</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" type="text/css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/callout.css" type="text/css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/styles.css" type="text/css" rel="stylesheet">
</head>

<body class="d-flex flex-column h-100">
<jsp:include page="fragment/header.jspf"/>
<main role="main" class="flex-shrink-0">
    <div class="container">
        <div class="row bd-callout">
            <c:if test="${not empty requestScope.discoveries}">
                <c:forEach var="discovery" items="${requestScope.discoveries}">
                    <div class="col col-md-1 col-sm-2 col-4">
                        <a href="${pageContext.request.contextPath}/vote?discovery_id=${discovery.id}&vote=VOTE_UP" class="btn btn-block btn-primary btn-sm">
                            <img src="${pageContext.request.contextPath}/resources/icons/arrow-up.svg" alt="" width="20" height="20" title="Arrow up">
                        </a>
                        <div class="card text-center"><c:out value="${discovery.upVote - discovery.downVote}"/></div>
                        <a href="${pageContext.request.contextPath}/vote?discovery_id=${discovery.id}&vote=VOTE_DOWN"
                           class="btn btn-block btn-primary btn-success btn-sm">
                            <img src="${pageContext.request.contextPath}/resources/icons/arrow-down.svg" alt="" width="20" height="20" title="Arrow down">
                        </a>
                    </div>
                    <div class="col col-md-11 col-sm-10 col-8">
                        <h3 class="align-content-center"><a href="<c:out value="${discovery.url}" />"><c:out value="${discovery.name}"/></a></h3>
                        <h6>Dodane przez: <c:out value="${discovery.user.username}"/>,
                            <fmt:formatDate value="${discovery.timestamp}" pattern="dd/MM/YYYY"/></h6>
                        <p><c:out value="${discovery.description}"/></p>
                        <a href="<c:out value="${discovery.url}" />" class="btn btn-default btn-xs">Przejdź do strony</a>
                    </div>
                </c:forEach>
            </c:if>
        </div>

    </div>
</main>
<jsp:include page="fragment/footer.jspf"/>
<script src="http://code.jquery.com/jquery-1.11.2.min.js"></script>
<script src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/bootstrap.bundle.min.js"></script>
</body>
</html>