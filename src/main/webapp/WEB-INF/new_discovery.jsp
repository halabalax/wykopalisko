<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html class="h-100" lang="pl">
<head>
    <title>Wykopalisko</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" type="text/css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/callout.css" type="text/css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/styles.css" type="text/css" rel="stylesheet">
</head>

<body class="d-flex flex-column h-100">
<jsp:include page="fragment/header.jspf"/>
<main role="main" class="flex-shrink-0">
    <div class="container">
        <div class="col-md-8 offset-md-2">
            <form class="form-signin text-center" action="add" method="post">
                <h1 class="h3 mb-3 font-weight-normal">Dodaj nowe wykopalisko</h1>
                <div class="form-group">
                    <label class="sr-only" for="inputName">Tytuł znaleziska</label>
                    <input name="inputName" id="inputName" type="text" class="form-control" placeholder="Tutuł znaleziska"
                           required autofocus/>
                    <label class="sr-only" for="inputUrl">Url</label>
                    <input name="inputUrl" id="inputUrl" type="url" class="form-control" placeholder="Url" required
                           autofocus/>
                    <label class="sr-only" for="inputDescription">Opis</label>
                    <textarea name="inputDescription" id="inputDescription" rows="5" class="form-control" placeholder="Opis"
                              required autofocus></textarea>
                </div>
                <button class="btn btn-block btn-lg btn-primary" type="submit">Dodaj</button>
            </form>
        </div>
    </div>
</main>
<jsp:include page="fragment/footer.jspf"/>
<script src="http://code.jquery.com/jquery-1.11.2.min.js"></script>
<script src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/bootstrap.bundle.min.js"></script>
</body>
</html>